Subject: [FYI] Performance hacks for broken Cell CPU revisions

The currently used CPUs of the Cell BE DD2.0 release have
a few glitches that may result in bad performance, these
workarounds are suggested in the Errata:

 - When a decrementer or external exception occurs,
   wake up the other SMT thread from pause(0) state.
 - When a system reset exception occurs (i.e. the CPU
   wakes up from pause(0) state), do a quick check if
   we were meant to wake up, otherwise go back to
   pause(0) state.
 - change the default floating point exception mode
   to zero.

It is rather unlikely that we find a way to create
a patch that won't be harmful to any other platform,
so this is definitely not meant for inclusion in the
mainline kernel, but only provided for those people
that build a special kernel for Cell BE DD2.0.

From: Jens Osterkamp <jens.osterkamp@de.ibm.com>
Signed-off-by: Arnd Bergmann <arndb@de.ibm.com>
---

Index: linus-2.6/arch/powerpc/kernel/head_64.S
===================================================================
--- linus-2.6.orig/arch/powerpc/kernel/head_64.S
+++ linus-2.6/arch/powerpc/kernel/head_64.S
@@ -316,6 +316,38 @@ label##_pSeries:					\
 	mtspr	SPRN_SPRG1,r13;		/* save r13 */	\
 	EXCEPTION_PROLOG_PSERIES(PACA_EXGEN, label##_common)
 
+#define WORKAROUND_EXCEPTION_PSERIES(n, label)			\
+	. = n;						\
+	.globl label##_pSeries;				\
+label##_pSeries:					\
+	mtspr	SPRN_SPRG1,r21;		/* save r21 */	\
+        mfspr   r21, SPRN_CTRLF;			\
+        oris    r21, r21, 0x00C0;			\
+        mtspr   SPRN_CTRLT, r21;			\
+	mfspr	r21,SPRN_SPRG1;		/* restore r21 */	\
+	mtspr	SPRN_SPRG1,r13;		/* save r13 */	\
+	EXCEPTION_PROLOG_PSERIES(PACA_EXGEN, label##_common)
+
+#define RES_EXCEPTION_PSERIES(n, label)			\
+	. = n;						\
+	.globl label##_pSeries;				\
+label##_pSeries:					\
+        mtspr   SPRN_SPRG2,r20;		/* use SPRG2 as scratch reg */ \
+        mtspr   SPRN_SPRG1,r21;		/* use SPRN_SPRG1 as scratch reg */ \
+        mfspr   r20,SPRN_SPRG3;		/* get paca virtual address */ \
+        cmpdi   r20,0x0;                /* if SPRG3 zero,thread  */    \
+	bne     20f;			/* shouldn't run */ \
+18:					/* Stop current Thread */ \
+	andi.	 r21,0,0;					 \
+	mtspr    SPRN_CTRLT,r21;					 \
+19:								\
+	b	 19b;		 	/* Thread should be stopped */	 \
+20:							\
+	HMT_MEDIUM;					\
+	mtspr	SPRN_SPRG1,r13;		/* save r13 */	\
+	RUNLATCH_ON(r13);				\
+	EXCEPTION_PROLOG_PSERIES(PACA_EXGEN, label##_common)
+
 #define HSTD_EXCEPTION_PSERIES(n, label)		\
 	. = n;						\
 	.globl label##_pSeries;				\
@@ -445,7 +477,11 @@ END_FTR_SECTION_IFSET(CPU_FTR_CAN_NAP)
 	.globl __start_interrupts
 __start_interrupts:
 
+#ifdef CONFIG_BE_DD2
+	RES_EXCEPTION_PSERIES(0x100, system_reset)
+#else
 	STD_EXCEPTION_PSERIES(0x100, system_reset)
+#endif
 
 	. = 0x200
 _machine_check_pSeries:
@@ -520,11 +556,19 @@ instruction_access_slb_pSeries:
 	mfspr	r12,SPRN_SRR1		/* and SRR1 */
 	b	.slb_miss_realmode	/* Rel. branch works in real mode */
 
+#ifdef CONFIG_BE_DD2
+	WORKAROUND_EXCEPTION_PSERIES(0x500, hardware_interrupt)
+#else
 	STD_EXCEPTION_PSERIES(0x500, hardware_interrupt)
+#endif
 	STD_EXCEPTION_PSERIES(0x600, alignment)
 	STD_EXCEPTION_PSERIES(0x700, program_check)
 	STD_EXCEPTION_PSERIES(0x800, fp_unavailable)
+#ifdef CONFIG_BE_DD2
+	WORKAROUND_EXCEPTION_PSERIES(0x900, decrementer)
+#else
 	STD_EXCEPTION_PSERIES(0x900, decrementer)
+#endif
 	STD_EXCEPTION_PSERIES(0xa00, trap_0a)
 	STD_EXCEPTION_PSERIES(0xb00, trap_0b)
 
Index: linus-2.6/arch/powerpc/platforms/cell/Kconfig
===================================================================
--- linus-2.6.orig/arch/powerpc/platforms/cell/Kconfig
+++ linus-2.6/arch/powerpc/platforms/cell/Kconfig
@@ -1,6 +1,16 @@
 menu "Cell Broadband Engine options"
 	depends on PPC_CELL
 
+config BE_DD2
+	bool "CPU bug workarounds for Cell Broadband Engine DD2.0"
+	depends on PPC_CELL
+	help
+	  This enables workarounds that let version DD2.0 of the
+	  Cell BE processor run at faster speed. These can have
+	  negative effects when running on other Cell BE versions
+	  or may crash other CPUs.
+	  Say 'n' here unless you expect to run on DD2.0 only.
+
 config SPU_FS
 	tristate "SPU file system"
 	default m
