# basetag: LINUX_2_6_16_BASE
# branch: BRANCH_LINUX_2_6_16


################################################################
# patches based on 2.6.16-arnd2
#  and some patches are replaced to 2.6.17-arnd10's one.

#######################################
# upstream patches, already cared for
#######################################

# Ryans hvc stuff
cell-support/2.6.16-arnd2/hvc-console-rework-4.diff
cell-support/2.6.16-arnd2/hvc-console-rtas-4.diff

# important patches
cell-support/2.6.16-arnd2/cell-detect.diff
cell-support/2.6.16-arnd2/iommu-fix.diff
cell-support/2.6.16-arnd2/cell-defconfigs-11.diff

# *** interrupt controller stuff
cell-support/2.6.16-arnd2/cell-pic-updates-3.diff

# syscall from spu patches
cell-support/2.6.16-arnd2/powerpc-asm-syscalls-h-2.diff
cell-support/2.6.16-arnd2/spufs-callbacks-3.diff
cell-support/2.6.16-arnd2/unshare-decl.diff

# ps mapping & mfc
cell-support/2.6.16-arnd2/spufs-mfc-file.diff
cell-support/2.6.16-arnd2/spufs-ps-mapping-3.diff 
cell-support/2.6.16-arnd2/spufs-ini-section.diff
cell-support/2.6.16-arnd2/init_mfc.diff
cell-support/2.6.16-arnd2/spufs-ls-protfault-fix.diff

cell-support/2.6.16-arnd2/spidernet-buildfix.diff
cell-support/2.6.16-arnd2/spidernet-selectfwloader.diff
cell-support/2.6.16-arnd2/spidernet-txcsum-2.diff
cell-support/2.6.16-arnd2/spidernet-rxramfl.diff

cell-support/2.6.16-arnd2/iic-map-guarded.diff
cell-support/2.6.16-arnd2/spider-pic-nodeid.diff
cell-support/2.6.16-arnd2/signal-fix.diff

# 2.6.16 specific
cell-support/2.6.16-arnd2/defconfig-update.diff

##############################
# unmerged stuff starts here
##############################

# Spidernet hack for new phy
#(not applied)spidernet-bcm5461-2.diff

# infiniband update, on its way upstream
# ib-max_rbc_rrs.diff

# Eric's systemsim tree
cell-support/2.6.16-arnd2/systemsim-base.diff
cell-support/2.6.16-arnd2/systemsim-block.diff
cell-support/2.6.16-arnd2/systemsim-bd-fixup.diff
cell-support/2.6.16-arnd2/systemsim-net.diff
cell-support/2.6.16-arnd2/systemsim-defconfig.diff
cell-support/2.6.16-arnd2/hvc-console-fss-2.diff

# spufs fixes
cell-support/2.6.16-arnd2/mss-map.diff

# *** currently broken
# mambo-spufs-timeslice.diff
#(not applied)systemsim-idle.diff
#(not applied)systemsim_idlefix.diff

#(not applied)cell-iic-cleanup.diff
#(not applied)spidernet-gbeburst.diff

# *** hacks for old firmware and hardware
cell-support/2.6.16-arnd2/dd2-performance.diff
#(not applied)dd2-hack-runlatch-hack.diff
#(not applied)pci-fixup-hack.diff
cell-support/2.6.16-arnd2/parm-fixup.diff

# *** make spufs work with NUMA and sparsemem
cell-support/2.6.16-arnd2/spufs-sparsemem-extreme-2.diff
#(not applied)memory-add.diff

# *** hash page fix
cell-support/2.6.16-arnd2/spu-hash-page-fix-2.diff

# *** 64 k pages, WIP
#(old)cell-support/2.6.16-arnd2/spu-base-no-module.diff
cell-support/2.6.17-arnd10/spu-base-no-module-2.diff
#(not applied)64-k-page-cell-3.diff
cell-support/2.6.16-arnd2/64k-page-enable.diff
cell-support/2.6.16-arnd2/fix-tlbie-64k-page.diff
#(not applied)spufs-64-k-fix.diff
#(not applied)64k-page-exports.diff
#(not applied)spufs-smm-hid.diff
cell-support/2.6.16-arnd2/spufs-64k-csa.diff

# *** enable all of the above
#(not applied)cbesim-defconfig.diff
#(not applied)defconfig-numa+64k.diff

# *** trivial fixes, could go upstream
cell-support/2.6.16-arnd2/spufs-decrementer-fix.diff
cell-support/2.6.16-arnd2/spufs-phys-id.diff
# systemsim-cell-detect.diff
cell-support/2.6.16-arnd2/spufs-fixme.diff

# *** spufs fixes from jeremy
#(not applied)spufs-kzalloc.diff
#(not applied)spufs-node-to-nid.diff
#(not applied)spufs-numa-id.diff
cell-support/2.6.16-arnd2/spufs-register-sysdev.diff

# not recommended
# cell-fake-numa.diff


################################################################

cell-support/06.03.24/07-powerpc-work-around-a-cell-interrupt-HV-bug.patch

cell-support/06.04.07/03-spufs-set-up-correct-SLB-entries-for-64k-pages.patch
cell-support/06.04.07/04-powerpc-export-symbols-for-page-size-selection.patch

cell-support/2.6.17-arnd10/spufs-rmdir-3.diff
cell-support/2.6.17-arnd10/spufs-initial-wbox-stat.diff
cell-support/2.6.17-arnd10/spufs-kzalloc--refresh.diff
cell-support/2.6.17-arnd10/spufs-channel-1-count.diff
cell-support/2.6.17-arnd10/spufs-fix-remove-stop_code-member-of-struct-spu.diff
cell-support/2.6.17-arnd10/spufs-fix-clean-_dump.h.diff
cell-support/2.6.17-arnd10/spufs-fix-class2-clear-stat-before-wakeup.diff
cell-support/2.6.17-arnd10/spufs-check-flags.diff
cell-support/2.6.17-arnd10/spufs-map-guarded.diff
cell-support/2.6.17-arnd10/spufs-dma-status.diff
cell-support/2.6.17-arnd10/spufs-ctx-kzalloc.diff
cell-support/2.6.17-arnd10/alp-remove-null-setup-cpu--refresh.patch

# under work: DMA Exception event support
cell-support/2.6.17-arnd10/spufs-fix-context-switch-during-fault.diff
cell-support/2.6.17-arnd10/spufs-correct-dma-exceptions.diff
cell-support/2.6.17-arnd10/spufs-dma-events-2.diff

# under work: gang interface
cell-support/2.6.17-arnd10/spufs-event-addon.diff
cell-support/2.6.17-arnd10/spufs-gang-2.diff

################################################################
# patches posted to ceb-oss-dev by arnd
#   at 04 Oct 2006 16:45:38.0058 (UTC)

cell-support/06.10.04/01-spufs-cell-spu-problem-state-mapping-updates--refresh.diff
cell-support/06.10.04/02-spufs-scheduler-support-for-NUMA--refresh.diff
#(already applied)cell-support/06.10.04/03-spufs-fic-context-switch-during-pagefault.diff
#(already applied)cell-support/06.10.04/04-spufs-implement-error-event-delivery-to-user-space.diff
#(already applied)cell-support/06.10.04/05-spufs-add-infrastracture-needed-for-gang-scheduling.diff
cell-support/06.10.04/06-spufs-use-correct-pg_prot-for-mapping-spu-LS.diff
cell-support/06.10.04/07-spufs-make-mailbox-functions-handle-multiple-element.diff
#(not applied)cell-support/06.10.04/08-spufs-remove-support-for-ancient-firmware.diff
cell-support/06.10.04/09-spufs-add-support-for-read-write-on-cntl.diff
#(not applied)cell-support/06.10.04/10-spufs-support-new-OF-device-tree-format.diff
cell-support/06.10.04/11-spufs-add-infrastructure-for-finding-elf-object.diff
spufs-notifier-2.6.26.diff
#(not applied)cell-support/06.10.04/12-powerpc-update-cell_defconfig.diff
#(not applied)cell-support/06.10.04/13-spiderpic-enable-new-style-devtree-support.diff
#(not applied)cell-support/06.10.04/14-cell-fix-bugs-found-by-sparse.diff


spufs-fix-cntl-close.diff

################################################################

cell-support/2.6.18-arnd5/spufs-signal2-fix.diff
cell-support/2.6.18-arnd5/spufs-mbox-fix-fix.diff
cell-support/other/spufs-always-map-local-store-non-guarded.patch

################################################################

spufs-cpu_affinity_set.diff
cell-support/other/spufs-fix-sdr1-access.patch 

powerpc/powerpc-possible-scheduler-deadlock.patch


#  OPEN-SOURCE PATCHES  /-------------
# ---------------------/  OUR PATCHES

# generic fixes
powerpc-fix-method-name-in-comment.diff

# gopi definitions
base-gopi-definition.diff

# platform support
base-basic-part.diff
reduce-kernel-size.diff
cbe-dd3-tb-bug-workaround.diff
rtc-flash.diff
base-pci.diff

# for kboot kernel
ps3pf-embed-initrd.diff
make-vmlinux.bin.diff

# virtual uart
vuart-interrupt.diff

# system manager
sysmgr-base.diff

# spufs
spufs-split-off-platform-code.patch
spufs-workaround-not-make-nid-node.diff
spufs-copy-from-platform-cell.diff
spufs-ignore-rtas.diff
base-sparsemem-expand-address-space.diff
spufs-base.diff
spufs-workaround-class0-interrupt.diff
spufs-callbaks-nodebug.diff

# logo ex
logo-chk-row.diff
logo-rework-1.diff
logo-rework-2.diff
logo-extra.diff
logo-extra-spufs.diff

# usb
usb-endian-conversion.diff

# gelic
gelic-base.diff

# ps3av
ps3av-base.diff

# ps3fb
ps3fb-base.diff

# ps3pf storage
ps3pf_storage_base.diff

# defconfig
integration-defconfigs.diff

# sound
ps3pf_sound_base.diff

# bluetooth
compat-bt-ioctl.diff
ps3pf-bt-hci.diff

# categorized  /---------------------
#-------------/  daily integration

rtc-flash-fix-deadlock.diff
base-basic-part-fix-rtc-boot-time.diff
base-basic-part-add-get_video_mode.diff
usb-add-ps3controller-quirk.diff
ps3pf-smt-performance-bugfix.diff
usb-workaround-ehci-iso.diff
