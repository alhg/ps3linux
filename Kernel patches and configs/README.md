# ps3linux - an independent repository for PS3 Linux & Cell development

---

![Alt text](https://aethyx.eu/wp-content/uploads/linuxonps3.jpg?raw=true "PS3 Linux FTW!")

---

This is the repository for the needed kernel patches from Linux version 2.6.x to 3.16.x.

One may obtain the individual patches by browsing the directories or may download the entire patch collection for a specific kernel version by downloading the according *.tar.gz.

There is a directory too which only contains the appropriate .config files needed for compiling a kernel. After patching, of course.

UPDATE OCTOBER 2018: 
From our experiences compiling PS3 Linux kernel 4.18 with these patches, 10 out of 28 DON'T WORK || NEED MODIFICATIONS, resulting in a not working (but booting) kernel. According to Geoff Levand it should be possible to boot kernels >= 4.17 again, see official announcement: https://mirrors.edge.kernel.org/pub/linux/kernel/people/geoff/cell/README, which we can confirm.

We tried to compile kernel 4.18, with and without patches, and our machines did boot. :-) 
Unfortunately it wasn't possible for unpatched kernels to find the needed ps3ddx partition. :-(
We then patched these kernels but a lot of essential patches didn't work anymore, which was expected of course. 
See our ps3linux-kernel-patches-418 folder for a HunkFailsLog as well as the according .rej files.

Let's work together now to get kernel 4.1x work again on our PS3s, it's doable! :-)

---

## Abstract

Browse the repository and watch carefully what you're looking for. Sometimes there exist README.md files, read them thoroughly.

Unfortunately patches for the official kernel tree seem to stop with version 3.16. But these shouldn't be needed anymore.

There exists a PS3 kernel tree still today: [https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git](https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git "https://git.kernel.org/pub/scm/linux/kernel/git/geoff/ps3-linux.git") maintained by Geoff Levand.
Be aware that due to a ABI bug the most recent kernel is 3.15 you may use on your own PS3 Linux machine (status: December 2017).

Feel free to contribute!

---

## Motivation

We at AETHYX MEDIAE are still trying to program internally for the Cell Broadband Engine. It's a side project.
Thus, from time to time we'll bring back stuff online we used in the past. Also docs and developer files are planned.
Thirdly, plans for publishing our stuff in the near future are under (heavy) construction.

---

## Contact

Feel free to drop us a line anytime: info [at] aethyx [dot] eu.

Thanks and have a good time!

---

## Donations

Setting up and creating this repository was a hell of work and consumed a lot of time and nerves, so donations are warmly welcome.

Please use the following addresses to send us donations:

BTC: 1Df8eapdFuxPxteZLfXnwvyVuo574psxKM

ETH: 0x0D2Ab63dfe70a7fA12f9d66eCfEA9dDc8F5173A8

XEM: NBZPMU-XES6ST-ITEBR3-IHAPTR-APGI3Y-RAAMHV-VZFJ
